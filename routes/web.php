<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=>['auth', 'owner:owner']], function(){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('user/{username}', 'HomeController@viewProfile')->name('user.view');
    Route::put('user/update/{id}', 'HomeController@userUpdate')->name('user.update');
    Route::post('user/search', 'SearchUserController@search')->name('user.search');

    Route::get('friend/request/add/{user_id}', 'AddFriendController@createFriendRequest')->name('request.add');
    Route::get('friend/request/accept/{friend_request_id}', 'AddFriendController@addFriend')->name('request.accept');

    Route::post('message/new', 'MessageController@newMessage')->name('message.new');
    Route::get('message/view/{id}', 'MessageController@viewMessages')->name('message.view');

    Route::post('message/send', 'MessageController@sendMessage')->name('message.send');

    Route::post('post/create', 'PostController@create')->name('post.create');
    Route::post('post/comment/{post_id}', 'PostController@comment')->name('post.comment');
    Route::get('post/view/{post_id}', 'PostController@view')->name('post.view');

    Route::get('notification/view/{notification_id}', 'NotificationController@view')->name('notification.view');

    Route::post('report/user/create/{reported_user_id}', 'ReportController@reportUser')->name('report.user');

});

Route::group(['middleware'=>['auth', 'admin:administrator']], function(){

    Route::get('admin/home', 'AdminHomeController@index')->name('admin.home');

    Route::get('admin/user/view/{username}', 'AdminHomeController@viewUser')->name('admin.view.user');
    Route::get('admin/user/deactivate/{user_id}', 'AdminHomeController@deactivateUser')->name('admin.user.deactivate');
    Route::get('admin/user/activate/{user_id}', 'AdminHomeController@activateUser')->name('admin.user.activate');
    Route::get('admin/user/activate/{user_id}', 'AdminHomeController@activateUser')->name('admin.user.activate');

    Route::delete('admin/user/post/{post_id}', 'AdminHomeController@deletePost')->name('admin.post.delete');

});

Auth::routes();
