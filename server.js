// var express = require('express');
// var app = express();
// var server = require('http').createServer(app);
// var io = require('socket.io').listen(server);
//
// users = [];
// connections = [];
//
// server.listen(process.env.PORT || 3000);
// console.log("server running");
//
// io.sockets.on('connection', function(socket){
//     connections.push(socket);
//     console.log('Connected: %s sockets connected', connections.length);
// });
// var app = require('express')();
// var http = require('http').Server(app);
// var io = require('socket.io')(http);
// var Redis = require('ioredis');
// var redis = new Redis();
// redis.subscribe('chat', function(err, count) {
// });
// redis.on('message', function(channel, message) {
//     console.log('Message Recieved: ' + message);
//     message = JSON.parse(message);
//     io.emit(channel + ':' + message.event, message.data);
// });
// http.listen(3000, function(){
//     console.log('Listening on Port 3000');
// });

// var app = require('express')();
// var http = require('http').Server(app);
// var io = require('socket.io')(http);
// var Redis = require('ioredis');
// var redis = new Redis();
// redis.subscribe('chat', function(err, count) {
// });
// redis.on('message', function(channel, message) {
//     console.log('Message Recieved: ' + message);
//
//     io.emit(channel + ':' + message.event, message.data);
// });
// http.listen(3000, function(){
//     console.log('Listening on Port 3000');
// });
var server = require('http').Server();
var io = require('socket.io')(server);

var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('chat');

redis.on('message', function(channel, message){
    // console.log(channel, message);
    message = JSON.parse(message);
    console.log(channel + ':' + message.event + message.data);
    io.emit(channel + '.' + message.data.receiver_id + '.' + message.data.sender_id + ':' + message.event, message.data);

});

server.listen(3000);
