<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'message';
    protected $fillable = ['body', 'is_read', 'message_recipient_id'];

    public function message_recipient(){
        return $this->belongsTo('App\MessageRecipient', 'message_recipient_id', 'id');
    }
}
