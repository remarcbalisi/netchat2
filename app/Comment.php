<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';
    protected $fillable = ['body', 'post_id', 'commentator_id'];

    public function post(){
        return $this->belongsTo('App\Post', 'post_id', 'id');
    }

    public function commentator(){
        return $this->belongsTo('App\User', 'commentator_id', 'id');
    }
}
