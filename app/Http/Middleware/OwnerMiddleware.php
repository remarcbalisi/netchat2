<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class OwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if( Auth::user()->hasRole($role) == true ){
            // return redirect('home');
            return $next($request);
        }

        else{
            return redirect()->route('admin.home');
            // return $next($request);
        }
    }
}
