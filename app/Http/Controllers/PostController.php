<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use Auth;
use Illuminate\Support\Facades\Storage;
use Image;
use App\Comment;
use App\Notification;
use App\FriendRequest;
use App\MessageRecipient;

class PostController extends Controller
{
    public function create(Request $request){
        $new_post = new Post;
        $new_post->body = $request->input('message');
        $new_post->user_id = Auth::user()->id;

        //saving image
        $file = request()->file('image');

        if( $file ){
            // Store the thumbnail and get the name of the image
            $path = $file->storeAs(
                'images', 'status_' . Auth::user()->username . time() .'.' . $file->getClientOriginalExtension(), 'public'
            );

            // Save the thumb location to user profile image
            $url = Storage::url($path);
            $new_post->image = $url;

            // Resize the thumbnail into 164.5 164.5 force crop
            $path = storage_path('app/public/') . $path;
            $img = Image::make($path);
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
        }

        else{
            $new_post->image = 'no image';
        }

        $new_post->save();

        return redirect()->back();

    }

    public function comment(Request $request, $post_id){
        $new_comment = new Comment;
        $new_comment->body = $request->input('comment');
        $new_comment->post_id = $post_id;
        $new_comment->commentator_id = Auth::user()->id;
        $new_comment->save();


        if( $new_comment->post->user->id != Auth::user()->id ){
            $new_notification = new Notification;
            $new_notification->message = Auth::user()->name . " commented on your post";
            $new_notification->user_id = $new_comment->post->user->id;
            $new_notification->table = 'comment';
            $new_notification->table_column_id = $new_comment->id;
            $new_notification->save();
        }


        return redirect()->back();
    }

    public function view($post_id){
        // get all the friend request
        $friend_requests = FriendRequest::where([
            'request_for' => Auth::user()->id
        ])->get();
        // get all the messages
        $messages = MessageRecipient::where([
            'receiver_id' => Auth::user()->id
        ])->get();
        $notifications = Notification::where([
            'user_id' => Auth::user()->id
        ])->orderBy('id', 'desc')->get();
        $post = Post::where([
            'id' => $post_id
        ])->first();

        // this will contain all the messages which is not yet read by the user
        $new_messages = array();

        foreach( $messages as $message ){
            $unread_messages = $message->messages->where('is_read', false);

            foreach( $unread_messages as $unread ){

                if( $new_messages ){
                    $count = 0;
                    foreach( $new_messages as &$new_message ){
                        if( $new_message["sender"] == $unread->message_recipient->username ){
                            $new_message["count"]++;
                            $count = 1;
                        }
                    }

                    if( !$count ){
                        $data = [
                            "sender" => $unread->message_recipient->username,
                            "count" => 1
                        ];
                        array_push($new_messages, $data);
                    }
                }

                else{
                    $data = [
                        "sender" => $unread->message_recipient->username,
                        "count" => 1,
                        "name" => $unread->message_recipient->name,
                        "id" => $unread->message_recipient->id
                    ];
                    array_push($new_messages, $data);
                }


            }
        }

        return view('user-post')->with([
            'friend_requests' => $friend_requests,
            'messages' => $messages,
            'new_messages' => $new_messages,
            'notifications' => $notifications,
            'post' => $post
        ]);
    }
}
