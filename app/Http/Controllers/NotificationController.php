<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use App\Notification;
use App\FriendRequest;
use App\MessageRecipient;
use Auth;

class NotificationController extends Controller
{
    public function view($notification_id){
        $notification = Notification::where([
            'id' => $notification_id,
        ])->first();

        $notification->is_read = true;
        $notification->save();



        switch( $notification->table ){
            case 'comment':
            $comment = Comment::where([
                'id' => $notification->table_column_id
            ])->first();
            return redirect()->route('post.view', ['post_id' => $comment->post->id]);
            break;
        }
    }
}
