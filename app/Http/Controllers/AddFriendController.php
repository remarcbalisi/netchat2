<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\FriendRequest;
use App\User;

class AddFriendController extends Controller
{
    // this function will create friend request
    public function createFriendRequest($user_id){
        // $user_id parameter is the id of the user
        // which is being added by the authenticated user

        if( !Auth::user()->check_friendship($user_id) ){ //if the friend request is not create yet
            $new_friend_request = new FriendRequest;
            $new_friend_request->requestor = Auth::user()->id;
            $new_friend_request->request_for = $user_id;
            $new_friend_request->save();
        }

        // if friend request is already exist return to home
        return redirect('home');
    }

    // this function will create the friendship relationship between two users
    public function addFriend($friend_request_id)
    {
        // get the friendrequest created
        $friend_request = FriendRequest::find($friend_request_id);

        $requestor = User::find($friend_request->requestor);
        $request_for = User::find($friend_request->request_for);

        // create friend relationship between two users
        $requestor->addFriend($request_for);
        $request_for->addFriend($requestor);

        $friend_request->is_accepted = true;
        $friend_request->save();

        // after that, return to home
        return redirect('home');
    }
}
