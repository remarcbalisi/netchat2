<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Report;
use App\User;
use Auth;
use App\Role;
use App\Notification;

class ReportController extends Controller
{
    public function reportUser(Request $request, $reported_user_id){
        $reported_user = User::where([
            'id' => $reported_user_id
        ])->first();

        $new_report = new Report;
        $new_report->title = 'User ' . $reported_user->username . ' has been reported';
        $new_report->body = $request->input('report_message');
        $new_report->sender_id = Auth::user()->id;
        $new_report->save();



        $admin_role = Role::find(1);
        $admin_users = $admin_role->adminUsers;

        foreach( $admin_users as $admin ){
            $new_notification = new Notification;
            $new_notification->message = 'User ' . $reported_user->username . ' has been reported';
            $new_notification->user_id = $admin->id;
            $new_notification->table = 'report_message';
            $new_notification->table_column_id = $new_report->id;
            $new_notification->save();
        }

        return redirect()->back();
    }
}
