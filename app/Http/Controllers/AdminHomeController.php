<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Report;
use App\Notification;
use App\User;
use App\Post;

class AdminHomeController extends Controller
{
    public function index(){
        $report_messages = Report::get();
        $notifications = Notification::where([
            'user_id' => Auth::user()->id
            ])->orderBy('id', 'desc')->get();
        $users = User::get();
        return view('admin-home')->with([
            'report_messages' => $report_messages,
            'notifications' => $notifications,
            'users' => $users
        ]);
    }

    public function viewUser( $username ){
        $user = User::where([
            'username' => $username
        ])->first();

        $user_posts = Post::where([
            'user_id' => $user->id
        ])->orderBy('id', 'desc')->get();

        $report_messages = Report::get();

        $notifications = Notification::where([
            'user_id' => Auth::user()->id
            ])->orderBy('id', 'desc')->get();
        $users = User::get();

        return view('admin-user-profile')->with([
            'user' => $user,
            'report_messages' => $report_messages,
            'notifications' => $notifications,
            'users' => $users,
            'user_posts' => $user_posts
        ]);
    }

    public function activateUser($user_id){
        $user = User::where([
            'id' => $user_id
        ])->first();

        $user->is_activated = true;
        $user->save();

        return redirect()->back();
    }

    public function deactivateUser($user_id){
        $user = User::where([
            'id' => $user_id
        ])->first();

        $user->is_activated = false;
        $user->save();

        return redirect()->back();
    }

    public function updateUser(Request $request, $id){
        // get the use that is going to be updated
        $update_user = User::find($id);

        // if password input field is present, update it!
        if( $request->input('password') ){
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,'.$id,
                'username' => 'required|string|max:255|unique:users,username,'.$id,
                'password' => 'required|string|min:6|confirmed',
            ]);

            $update_user->password = bcrypt( $request->input('password') );
            $update_user->save();
        }

        //else do not update the password.
        else{
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,'.$id,
                'username' => 'required|string|max:255|unique:users,username,'.$id,
            ]);
        }

        $update_user->name = $request->input('name');
        $update_user->email = $request->input('email');
        $update_user->username = $request->input('username');

        //saving image
        $file = request()->file('image');

        if( $file ){
            // Store the thumbnail and get the name of the image
            $path = $file->storeAs(
                'images', 'thumb_' . Auth::user()->username . '.' . $file->getClientOriginalExtension(), 'public'
            );

            // Save the thumb location to user profile image
            $url = Storage::url($path);
            $update_user->image = $url;
            $update_user->save();

            // Resize the thumbnail into 164.5 164.5 force crop
            $path = storage_path('app/public/') . $path;
            $img = Image::make($path);
            $img->resize(164.5, 164.5, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
        }

        else{
            $update_user->save();
        }

        return redirect()->back();
    }

    public function deletePost($post_id){
        $post = Post::where([
            'id' => $post_id
        ])->first()->delete();

        return redirect()->back();
    }
}
