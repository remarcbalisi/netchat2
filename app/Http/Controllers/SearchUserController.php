<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\FriendRequest;
use Auth;
use App\MessageRecipient;
use App\Notification;

class SearchUserController extends Controller
{
    // this function will search all the user that will match the name requested
    // by the user
    public function search(Request $request)
    {
        $name = $request->input('name');
        $search_users = User::where('name', 'LIKE', "%$name%")->get();

        $friend_requests = FriendRequest::where([
            'request_for' => Auth::user()->id
        ]);
        $messages = MessageRecipient::where([
            'receiver_id' => Auth::user()->id
        ])->get();
        $notifications = Notification::where([
            'user_id' => Auth::user()->id
        ])->orderBy('id', 'desc')->get();

        $new_messages = array();

        foreach( $messages as $message ){
            $unread_messages = $message->messages->where('is_read', false);

            foreach( $unread_messages as $unread ){

                if( $new_messages ){
                    $count = 0;
                    foreach( $new_messages as &$new_message ){
                        if( $new_message["sender"] == $unread->message_recipient->username ){
                            $new_message["count"]++;
                            $count = 1;
                        }
                    }

                    if( !$count ){
                        $data = [
                            "sender" => $unread->message_recipient->username,
                            "count" => 1
                        ];
                        array_push($new_messages, $data);
                    }
                }

                else{
                    $data = [
                        "sender" => $unread->message_recipient->username,
                        "count" => 1,
                        "name" => $unread->message_recipient->name,
                        "id" => $unread->message_recipient->id
                    ];
                    array_push($new_messages, $data);
                }


            }
        }

        return view('user-search')->with([
            'search_users' => $search_users,
            'searched_user' => $name,
            'friend_requests' => $friend_requests,
            'messages' => $messages,
            'new_messages' => $new_messages,
            'notifications' => $notifications
        ]);
    }
}
