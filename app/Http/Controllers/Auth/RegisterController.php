<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:60|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return $new_user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'image' => '/images/default-photo.png',
            'password' => bcrypt($data['password']),
        ]);

    }

    // public function register(Request $request)
    // {
    //     $new_user = new User;
    //     $new_user->name = $request->input('name');
    //     $new_user->email = $request->input('email');
    //     $new_user->username = $request->input('username');
    //     $new_user->image = '/images/default-photo.png';
    //     $new_user->password = bcrypt($request->input('password'));
    //     $new_user->save();
    //
    //     $new_user->roles()->attach(2);
    //
    //     return redirect()->back()->with([
    //         'info' => 'Successfully Registered ' . $new_user->name
    //     ]);
    //
    // }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        event(new Registered($user));

        $user->roles()->attach(2);
        return redirect()->back()->with([
                'info' => 'Successfully Registered ' . $user->name
            ]);
    }
}
