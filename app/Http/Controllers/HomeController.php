<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\FriendRequest;
use App\Message;
use App\MessageRecipient;
use Auth;
use Image;
use App\Post;
use App\Notification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the friend request
        $friend_requests = FriendRequest::where([
            'request_for' => Auth::user()->id
        ])->get();
        // get all the messages
        $messages = MessageRecipient::where([
            'receiver_id' => Auth::user()->id
        ])->get();
        $notifications = Notification::where([
            'user_id' => Auth::user()->id
        ])->orderBy('id', 'desc')->get();

        // this will contain all the messages which is not yet read by the user
        $new_messages = array();

        foreach( $messages as $message ){
            $unread_messages = $message->messages->where('is_read', false);

            foreach( $unread_messages as $unread ){

                if( $new_messages ){
                    $count = 0;
                    foreach( $new_messages as &$new_message ){
                        if( $new_message["sender"] == $unread->message_recipient->sender->username ){
                            $new_message["count"]++;
                            $count = 1;
                        }
                    }

                    if( !$count ){
                        $data = [
                            "sender" => $unread->message_recipient->sender->username,
                            "count" => 1
                        ];
                        array_push($new_messages, $data);
                    }
                }

                else{
                    $data = [
                        "sender" => $unread->message_recipient->sender->username,
                        "count" => 1,
                        "name" => $unread->message_recipient->sender->name,
                        "id" => $unread->message_recipient->sender->id
                    ];
                    array_push($new_messages, $data);
                }


            }
        }

        return view('home')->with([
            'friend_requests' => $friend_requests,
            'messages' => $messages,
            'new_messages' => $new_messages,
            'notifications' => $notifications
        ]);
    }

    // this function will view the profile of the selected user
    public function viewProfile($username){

        // get the user that is going to be viewed
        $user = User::where([
            'username' => $username
        ])->first();

        $friend_requests = FriendRequest::where([
            'request_for' => Auth::user()->id
        ])->get();
        $messages = MessageRecipient::where([
            'receiver_id' => Auth::user()->id
        ])->get();
        $posts = Post::where([
            'user_id' => $user->id,
        ])->orderBy('id', 'desc')->get();
        $notifications = Notification::where([
            'user_id' => Auth::user()->id
        ])->orderBy('id', 'desc')->get();

        $new_messages = array();

        foreach( $messages as $message ){
            $unread_messages = $message->messages->where('is_read', false);

            foreach( $unread_messages as $unread ){

                if( $new_messages ){
                    $count = 0;
                    foreach( $new_messages as &$new_message ){
                        if( $new_message["sender"] == $unread->message_recipient->sender->username ){
                            $new_message["count"]++;
                            $count = 1;
                        }
                    }

                    if( !$count ){
                        $data = [
                            "sender" => $unread->message_recipient->username,
                            "count" => 1
                        ];
                        array_push($new_messages, $data);
                    }
                }

                else{
                    $data = [
                        "sender" => $unread->message_recipient->sender->username,
                        "count" => 1,
                        "name" => $unread->message_recipient->sender->name,
                        "id" => $unread->message_recipient->sender->id
                    ];
                    array_push($new_messages, $data);
                }


            }
        }

        return view('user-profile')->with([
            'user' => $user,
            'friend_requests' => $friend_requests,
            'messages' => $messages,
            'new_messages' => $new_messages,
            'posts' => $posts,
            'notifications' => $notifications
        ]);
    }

    // this function will update the authenticated user
    public function userUpdate(Request $request, $id){

        // get the user that is going to be updated
        $update_user = User::find($id);

        // if password input field is present, update it!
        if( $request->input('password') ){
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,'.$id,
                'username' => 'required|string|max:255|unique:users,username,'.$id,
                'password' => 'required|string|min:6|confirmed',
            ]);

            $update_user->password = bcrypt( $request->input('password') );
            $update_user->save();
        }

        //else do not update the password.
        else{
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,'.$id,
                'username' => 'required|string|max:255|unique:users,username,'.$id,
            ]);
        }

        $update_user->name = $request->input('name');
        $update_user->email = $request->input('email');
        $update_user->username = $request->input('username');

        //saving image
        $file = request()->file('image');

        if( $file ){
            // Store the thumbnail and get the name of the image
            $path = $file->storeAs(
                'images', 'thumb_' . Auth::user()->username . '.' . $file->getClientOriginalExtension(), 'public'
            );

            // Save the thumb location to user profile image
            $url = Storage::url($path);
            $update_user->image = $url;
            $update_user->save();

            // Resize the thumbnail into 164.5 164.5 force crop
            $path = storage_path('app/public/') . $path;
            $img = Image::make($path);
            $img->resize(164.5, 164.5, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
        }

        else{
            $update_user->save();
        }

        return redirect()->back();

    }
}
