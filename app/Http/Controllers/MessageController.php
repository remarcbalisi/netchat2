<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\MessageRecipient;
use App\Message;
use App\FriendRequest;
use Auth;
use App\Events\SendMessageEvent;
use App\Notification;

use Illuminate\Support\Facades\Redis;

class MessageController extends Controller
{
    // this function will create a new message, this is not the real time message
    public function newMessage(Request $request){

        // check the user if existed
        $check_user = User::where('email', $request->input('email'))->first();

        if( $check_user ){ //if existed
            $check_recipient = MessageRecipient::where([
                'sender_id' => Auth::user()->id,
                'receiver_id' => $check_user->id
            ])->first();

            // check message_recipient table if already exist
            if( $check_recipient ){
                $new_message = new Message;
                $new_message->body = $request->input('message');
                $new_message->message_recipient_id = $check_recipient->id;
                $new_message->save();


            }
            // if not exist
            else{
                $new_recipient = new MessageRecipient;
                $new_recipient->sender_id = Auth::user()->id;
                $new_recipient->receiver_id = $check_user->id;
                $new_recipient->save();

                $new_message = new Message;
                $new_message->body = $request->input('message');
                $new_message->message_recipient_id = $new_recipient->id;
                $new_message->save();

            }

            return redirect()->back()->with([
                'sent' => "Message Sent!"
            ]);
        }
        else{
            return redirect()->back()->with([
                'error' => "Recipient " . $request->input('email') . " doesn't exist!"
            ]);
        }
    }

    // this function will get all the messages between the two users
    public function viewMessages($recipient_id){
        $message_recipients = MessageRecipient::where([
            'sender_id' => Auth::user()->id,
            'receiver_id' => $recipient_id
        ])->orWhere([
            'sender_id' => $recipient_id,
            'receiver_id' => Auth::user()->id
            ])->get();
        $notifications = Notification::where([
            'user_id' => Auth::user()->id
        ])->orderBy('id', 'desc')->get();

        $messages_array = array();

        foreach( $message_recipients as $recipient ){

            $recipient_msgs = $recipient->messages;

            foreach( $recipient_msgs as $msgs ){
                $data = [
                    'id' => $msgs->id,
                    'body' => $msgs->body,
                    'is_read' => $msgs->is_read,
                    'sender' => $msgs->message_recipient->sender->name,
                    'created_at' => $msgs->created_at
                ];
                array_push( $messages_array, $data );

                // update the message and set is_read to true
                if(!$msgs->is_read){
                    $msgs->is_read = true;
                    $msgs->save();
                }

            }
        }

        sort($messages_array);

        $friend_requests = FriendRequest::where([
            'request_for' => Auth::user()->id
        ])->get();
        $messages = MessageRecipient::where([
            'receiver_id' => Auth::user()->id
        ])->get();

        $new_messages = array();

        foreach( $messages as $message ){
            $unread_messages = $message->messages->where('is_read', false);

            foreach( $unread_messages as $unread ){

                if( $new_messages ){
                    $count = 0;
                    foreach( $new_messages as &$new_message ){
                        if( $new_message["sender"] == $unread->message_recipient->sender->username ){
                            $new_message["count"]++;
                            $count = 1;
                        }
                    }

                    if( !$count ){
                        $data = [
                            "sender" => $unread->message_recipient->sender->username,
                            "count" => 1
                        ];
                        array_push($new_messages, $data);
                    }
                }

                else{
                    $data = [
                        "sender" => $unread->message_recipient->sender->username,
                        "count" => 1,
                        "name" => $unread->message_recipient->sender->name,
                        "id" => $unread->message_recipient->id
                    ];
                    array_push($new_messages, $data);
                }


            }
        }

        return view('user-chat')->with([
            'friend_requests' => $friend_requests,
            'messages' => $messages_array,
            'new_messages' => $new_messages,
            'recipient_id' => $recipient_id,
            'notifications' => $notifications
        ]);
    }

    // this function will create a message that is in real time
    public function sendMessage(Request $request){

        //check user if exist
        $check_user = User::where('id', $request->recipient_id)->first();

        if( $check_user ){
            $check_recipient = MessageRecipient::where([
                'sender_id' => Auth::user()->id,
                'receiver_id' => $check_user->id
            ])->first();

            if( $check_recipient ){
                $new_message = new Message;
                $new_message->body = $request->message;
                $new_message->message_recipient_id = $check_recipient->id;
                $new_message->save();

                // this event will broadcast a channel and
                // the message data, recipient id, sender id, and sender name
                event( new SendMessageEvent($new_message, $check_user->id, Auth::user()->id, Auth::user()->name) );
                return response()->json($new_message);
            }
            else{
                $new_recipient = new MessageRecipient;
                $new_recipient->sender_id = Auth::user()->id;
                $new_recipient->receiver_id = $check_user->id;
                $new_recipient->save();

                $new_message = new Message;
                $new_message->body = $request->message;
                $new_message->message_recipient_id = $new_recipient->id;
                $new_message->save();

                // this event will broadcast a channel and
                // the message data, recipient id, sender id, and sender name
                event( new SendMessageEvent($new_message, $check_user->id, Auth::user()->id, Auth::user()->name) );
                return response()->json($new_message);
            }

        }
        else{
            return redirect()->back()->with([
                'error' => "Recipient " . $request->input('email') . " doesn't exist!"
            ]);

            $response = array(
                'error' => 'Recipient does not exist!'
            );

            return response()->json($response);
        }
    }

}
