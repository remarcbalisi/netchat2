<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Auth;
use App\FriendRequest;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function friends()
	{
		return $this->belongsToMany('App\User', 'user_friend', 'user_id', 'friend_id');
	}

	public function addFriend(User $user)
	{
		$this->friends()->attach($user->id);
	}

	public function removeFriend(User $user)
	{
		$this->friends()->detach($user->id);
	}

    public function check_friendship($user_id){
        $friend_request = FriendRequest::where([
            'requestor' => Auth::user()->id,
            'request_for' => $user_id
        ])->orWhere([
            'requestor' => $user_id,
            'request_for' => Auth::user()->id
        ])->first();

        return $friend_request;
    }

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function hasRole($role_name){

        foreach($this->roles as $role){
            if( $role->name == $role_name ){
                return true;
            }
            else{
                return false;
            }
        }
    }

}
