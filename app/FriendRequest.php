<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FriendRequest extends Model
{
    protected $table = 'friend_request';
    protected $fillable = ['requestor', 'request_for', 'is_accepted'];

    public function requestor_model(){
        return $this->belongsTo('App\User', 'requestor', 'id');
    }

    public function request_for_model(){
        return $this->belongsTo('App\User', 'request_for', 'id');
    }
}
