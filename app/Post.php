<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';
    protected $fillable = ['body', 'user_id', 'image'];

    public function user()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

    public function comments(){
        return $this->hasMany('App\Comment')->orderBy('id', 'desc');
    }
}
