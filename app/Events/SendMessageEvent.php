<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\User;
use App\Message;

class SendMessageEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;


    public $data;
    public $receiver_id;
    public $sender_id;
    public $sender_name;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Message $data, $receiver_id, $sender_id, $sender_name)
    {
        $this->data = $data;
        $this->receiver_id = $receiver_id;
        $this->sender_id = $sender_id;
        $this->sender_name = $sender_name;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('chat');
        // return ['chat'.$this->receiver_id];
    }
}
