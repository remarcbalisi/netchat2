<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';
    protected $fillable = ['message', 'user_id', 'table', 'table_column_id', 'is_read'];

    public function user(){
        $this->belongsTo('App\User', 'user_id', 'id');
    }
}
