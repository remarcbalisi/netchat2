<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageRecipient extends Model
{
    protected $table = 'message_recipient';
    protected $fillable = ['sender_id', 'receiver_id'];

    public function sender(){
        return $this->belongsTo('App\User', 'sender_id', 'id');
    }

    public function receiver(){
        return $this->belongsTo('App\User', 'receiver_id', 'id');
    }

    public function messages(){
        return $this->hasMany('App\Message');
    }
}
