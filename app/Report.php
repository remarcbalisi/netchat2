<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'report_message';
    protected $fillable = ['title', 'body', 'is_read', 'sender_id'];

    public function sender(){
        return $this->belongsTo('App\User', 'sender_id', 'id');
    }
}
