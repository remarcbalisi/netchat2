@extends('layouts.app')

@section('content')

                <div class="panel-body">
                    <p style="font-size:17px">Search result for " {{ $searched_user }} "</p>

                    @foreach( $search_users as $search_user )

                        @if( $search_user->id == Auth::user()->id )
                        <div class="links">
                            <hr>
                            <p>This is you!</p>
                            <br>
                                <div class="row">
                                    <a href="{{route('user.view', ['username'=>$search_user->username])}}">
                                        <div class="col-md-4">
                                            <img style="display:inline-block" src="{{url($search_user->image)}}" alt="">
                                        </div>
                                        <div class="col-md-6">

                                                <h3 style="display:inline-block">{{$search_user->name}}</h3>
                                                <p>{{$search_user->email}}</p>

                                        </div>
                                    </a>

                                    <div class="col-md-2">

                                    </div>
                                </div>
                            <hr>
                        </div>
                        @else
                        <div class="links">
                            <hr>
                                <div class="row">
                                    <a href="{{route('user.view', ['username'=>$search_user->username])}}">
                                        <div class="col-md-4">
                                            <img style="display:inline-block" src="{{url($search_user->image)}}" alt="">
                                        </div>
                                        <div class="col-md-6">

                                                <h3 style="display:inline-block">{{$search_user->name}}</h3>
                                                <p>{{$search_user->email}}</p>

                                        </div>
                                    </a>

                                    <div class="col-md-2">
                                        @if( !$search_user->check_friendship($search_user->id) )
                                        <a href="{{route('request.add', ['user_id'=>$search_user->id])}}"><button class="btn btn-primary" type="button" name="button">+Add Friend</button></a>
                                        @elseif($search_user->check_friendship($search_user->id)->request_for == Auth::user()->id && $search_user->check_friendship($search_user->id)->is_accepted == false)
                                        <a href="{{route('request.accept', ['friend_request_id'=>$search_user->check_friendship($search_user->id)->id])}}"><button class="btn btn-primary" type="button" name="button">+Accept</button></a>
                                        @elseif($search_user->check_friendship($search_user->id)->is_accepted == false)
                                            <p>&#10004;&nbsp;Friend Request Sent!</p>
                                        @elseif($search_user->check_friendship($search_user->id)->is_accepted == true)
                                            <p style="color:green">&#10004;&nbsp;Friends!</p>
                                        @endif
                                        <!-- {{$search_user->check_friendship($search_user->id)}} -->
                                    </div>
                                </div>
                            <hr>
                        </div>
                        @endif
                    @endforeach

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
