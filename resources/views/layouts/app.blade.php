<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('APP_NAME', 'NetChat') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/socket.io.js') }}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('APP_NAME', 'NetChat') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>

                                        <a href="{{route('user.view', ['username'=>Auth::user()->username])}}">
                                            <img style="vertical-align:top" width="20%" src="{{url(Auth::user()->image)}}" alt="">
                                            View Profile
                                        </a>

                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @if(Auth::check())
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="" action="{{route('user.search')}}" method="POST">
                                {{ csrf_field() }}
                                <div class="input-group">
                                  <input id="name" name="name" type="text" class="form-control" placeholder="Search on NetChat" aria-describedby="basic-addon1">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Search!</button>
                                  </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div style="height:480px;overflow-y: scroll;" class="panel panel-default">
                        <div class="panel-heading">Friends&nbsp;(&nbsp;&nbsp;{{Auth::user()->friends->count()}}&nbsp;&nbsp;)</div>

                        <div class="panel-body">
                            <div class="links">
                                <a href="{{route('home')}}">Create New Message</a>
                                <br>
                                <br>
                                @if(!Auth::user()->friends->count())
                                <p>You have no friends</p>
                                @endif
                                @foreach( Auth::user()->friends as $friend )
                                <a href="{{route('user.view', ['username'=>$friend->username])}}">{{$friend->name}}</a>
                                <a style="text-decoration:none" href="{{route('message.view', ['user_id'=>$friend->id])}}">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;View Messages</a>
                                <p style="font-size:12px;margin-top:-8px;">{{$friend->email}}</p>
                                <br>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">Dashboard</div>

                        <div class="panel-body">
                            <li style="list-style-type: none;display:inline-block">
                                <a href="{{route('home')}}">Home</a>
                            </li>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <li style="list-style-type: none;display:inline-block" class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Friend Requests&nbsp;(&nbsp;{{$friend_requests->where('is_accepted', false)->count()}}&nbsp;) <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        @if( !$friend_requests->where('is_accepted', false)->count() )
                                        <li><a href="#">You have no new Friend Request</a></li>
                                        <li class="divider"></li>
                                        @else
                                            @foreach($friend_requests->where('is_accepted', false) as $friend_request)
                                            <a href="{{route('user.view', ['username'=>$friend_request->requestor_model->username])}}">
                                            Friend request from {{$friend_request->requestor_model->name}}
                                            </a>
                                            @endforeach
                                            <li class="divider"></li>
                                        @endif

                                    </li>
                                    @foreach($friend_requests->where('is_accepted', true) as $friend_request)
                                    <li>
                                        <a style="color:green;" href="{{route('user.view', ['username'=>$friend_request->requestor_model->username])}}">
                                        &#10004;Friend request from {{$friend_request->requestor_model->name}}
                                        </a>
                                    </li>

                                    @endforeach
                                </ul>
                            </li>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <li style="list-style-type: none;display:inline-block" class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Inbox&nbsp;(&nbsp;<?php echo count($new_messages) ?>&nbsp;) <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    @if( !count($new_messages) )
                                    <li>
                                        <li><a href="#">No new Messages</a></li>
                                    </li>
                                    @endif
                                    @foreach($new_messages as $message )

                                        <li>
                                            <a href="{{route('message.view', ['user_id'=>$message['id']])}}">Message from {{$message["name"]}} (&nbsp;{{$message["count"]}}&nbsp;)</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <li style="list-style-type: none;display:inline-block" class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Notification&nbsp;(&nbsp;{{$notifications->where('is_read', false)->count()}}&nbsp;) <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    @if( !$notifications->where('is_read', false)->count() )
                                    <li>
                                        <a href="#">No new Notifications</a>
                                    </li>
                                    @endif
                                    @foreach($notifications as $notification )

                                    <li>
                                        <a href="{{route('notification.view', ['notification_id'=>$notification->id])}}">
                                            @if( $notification->is_read == true )
                                            &#10004;&nbsp;
                                            @endif
                                            {{$notification->message}}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        </div>

                        <br>
        @endif

        @yield('content')
    </div>

</body>
</html>
