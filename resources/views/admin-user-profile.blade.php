@extends('layouts.admin-app')

@section('content')

                <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="{{route('user.update', ['id'=>Auth::user()->id])}}">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img style="display:inline-block;vertical-align:top" src="{{ url($user->image) }}" alt="">
                            <br>
                            <label for="image" class="btn">Update Photo</label>
                            <input id="image" type="file" name="image" style="visibility:hidden;">
                            <h2 style="margin-top:-15px;">{{$user->name}}</h2>
                            <h5>{{$user->email}}</h5>
                            @if( $user->is_activated == true )
                            <a href="{{route('admin.user.deactivate', ['user_id'=>$user->id])}}">
                                <button id="report-user-btn" type="button" class="btn btn-danger btn-xs">Deactivate User</button>
                            </a>
                            @else
                            <a href="{{route('admin.user.activate', ['user_id'=>$user->id])}}">
                                <button id="report-user-btn" type="button" class="btn btn-success btn-xs">Activate User</button>
                            </a>
                            @endif

                        </div>

                        <div class="col-md-12">
                                {{ csrf_field() }}
                                {!! method_field('put') !!}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="control-label">Name</label>


                                        <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                        <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif

                                </div>

                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <label for="username" class="col-md-4 control-label">Username</label>

                                        <input id="username" type="text" class="form-control" name="username" value="{{ Auth::user()->username }}" required>

                                        @if ($errors->has('username'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                        <input id="password" type="password" class="form-control" name="password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>

                                <div class="form-group">
                                        <button type="submit" class="btn btn-primary">
                                            Update
                                        </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-4">
            <div class="panel panel-default">

                <div class="panel-body">
                    @foreach($user_posts as $post)
                    <div class="jumbotron">
                        <button style="margin-bottom:10px;" id="delete-post-btn" type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-post-modal-{{$post->id}}">Delete this Post</button>
                        <!-- Modal -->
                        <div id="delete-post-modal-{{$post->id}}" class="modal fade" role="dialog">
                          <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Are you sure you want to delete this post?</h4>
                              </div>
                              <div class="modal-body">
                                <div class="jumbotron">
                                    @if( $post->image == 'no image' )

                                    @else
                                    <center>
                                        <img src="{{url($post->image)}}" alt="">
                                    </center>
                                    @endif

                                    <p style="font-size:25px;">{{$post->body}}</p>
                                    <p style="font-size:12px;margin-top:-23px;">{{ date('F d, Y', strtotime($post->created_at)) }}</p>
                                    <form class="" action="{{route('admin.post.delete', ['post_id'=>$post->id])}}" method="post">
                                        {{ csrf_field() }}
                                        {!! method_field('delete') !!}
                                        <button type="submit" class="btn btn-success">Yes</button>
                                    </form>
                                </div>
                              </div>
                              <div class="modal-footer">

                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>

                          </div>
                        </div>

                        @if( $post->image == 'no image' )

                        @else
                        <center>
                            <img src="{{url($post->image)}}" alt="">
                        </center>
                        @endif

                        <p style="font-size:25px;">{{$post->body}}{{$post->image == 'no image'}}</p>
                        <p style="font-size:12px;margin-top:-23px;">{{ date('F d, Y', strtotime($post->created_at)) }}</p>

                        <form class="" action="{{route('post.comment', ['post_id'=>$post->id])}}" method="POST">
                            {{ csrf_field() }}
                            <div class="input-group">
                              <input id="comment" name="comment" type="text" class="form-control" placeholder="Write a comment" aria-describedby="basic-addon1">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Comment</button>
                              </span>
                            </div>
                        </form>
                        <br>
                        <br>
                        <div style="padding:5px;max-width:598px;max-height:184px;overflow-y:scroll" class="comment-box">
                            <br>
                            @foreach( $post->comments as $comment )
                            @if($comment->commentator->id == Auth::user()->id)
                            <p style="font-size:15px;margin-top:-20px;"><strong>You says: &nbsp;</strong>{{$comment->body}}</p>
                            @else
                            <p style="font-size:15px;margin-top:-20px;"><strong>{{$comment->commentator->name}} says: &nbsp;</strong>{{$comment->body}}</p>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>

                <br>
            </div>
        </div>
    </div>
</div>


@endsection
