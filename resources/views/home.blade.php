@extends('layouts.app')

@section('content')


                <div class="panel-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{route('message.new')}}">
                        {{ csrf_field() }}

                        @if (Session::has('sent'))
                            <p class="alert alert-success">
                                <strong>{{ Session::get( 'sent' ) }}</strong>
                            </p>
                        @elseif (Session::has('error'))
                            <p class="alert alert-danger">
                                <strong>{{ Session::get( 'error' ) }}</strong>
                            </p>
                        @endif
                        <br>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Reciepient email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">

                            <div class="col-md-6">
                                <textarea id="message"  class="form-control" name="message" value="{{ old('message') }}" required rows="8" cols="80" placeholder="Type your message here"></textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send
                                </button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
