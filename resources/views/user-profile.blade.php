@extends('layouts.app')

@section('content')

                @if( Auth::user()->id == $user->id )
                <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="{{route('user.update', ['id'=>Auth::user()->id])}}">
                @endif

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img style="display:inline-block;vertical-align:top" src="{{ url($user->image) }}" alt="">
                            @if( Auth::user()->id == $user->id ) <!--begin: if statement in checking if auth-user == user -->
                            <br>
                            <label for="image" class="btn">Update Photo</label>
                            <input id="image" type="file" name="image" style="visibility:hidden;">
                            <h2 style="margin-top:-15px;">{{$user->name}}</h2>
                            <h5>{{$user->email}}</h5>
                            @else
                            <h2 style="margin-top:10px;">{{$user->name}}</h2>
                            <h5>{{$user->email}}</h5>
                                @if( !$user->check_friendship($user->id) )
                                <a href="{{route('request.add', ['user_id'=>$user->id])}}"><button class="btn btn-primary" type="button" name="button">+Add Friend</button></a>
                                @elseif($user->check_friendship($user->id)->request_for == Auth::user()->id && $user->check_friendship($user->id)->is_accepted == false)
                                <a href="{{route('request.accept', ['friend_request_id'=>$user->check_friendship($user->id)->id])}}"><button class="btn btn-primary" type="button" name="button">+Accept</button></a>
                                @elseif($user->check_friendship($user->id)->is_accepted == false)
                                    <p>&#10004;&nbsp;Friend Request Sent!</p>
                                @elseif($user->check_friendship($user->id)->is_accepted == true)
                                    <p style="color:green">&#10004;&nbsp;Friends!</p>
                                    <button id="report-user-btn" type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#report-user-modal">Report User</button>
                                @endif

                                <!-- Modal -->
                                <div id="report-user-modal" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Report {{$user->name}}</h4>
                                      </div>
                                      <div class="modal-body">
                                        <form class="" action="{{route('report.user', ['reported_user_id'=>$user->id])}}" method="post">
                                            {{ csrf_field() }}
                                            <textarea class="form-control" name="report_message" rows="8" cols="80" placeholder="Type your message here" required></textarea>
                                            <br>
                                            <button class="btn btn-primary" type="submit" name="button">Submit</button>
                                        </form>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>


                        </div>

                        <div class="col-md-8">
                            <form style="margin-left:50px;" class="form-horizontal" role="form" method="POST" action="{{route('message.new')}}">
                                {{ csrf_field() }}

                                @if (Session::has('sent'))
                                    <p class="alert alert-success">
                                        <strong>{{ Session::get( 'sent' ) }}</strong>
                                    </p>
                                @elseif (Session::has('error'))
                                    <p class="alert alert-danger">
                                        <strong>{{ Session::get( 'error' ) }}</strong>
                                    </p>
                                @endif
                                <br>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required autofocus placeholder="Reciepient email">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">

                                    <div class="col-md-6">
                                        <textarea id="message"  class="form-control" name="message" value="{{ old('message') }}" required rows="8" cols="80" placeholder="Type your message here"></textarea>
                                        @if ($errors->has('message'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('message') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Send
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @endif<!--end: if statement in checking if auth-user == user -->

                        @if( Auth::user()->id == $user->id )
                        <div class="col-md-12">
                                {{ csrf_field() }}
                                {!! method_field('put') !!}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="control-label">Name</label>


                                        <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                        <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif

                                </div>

                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <label for="username" class="col-md-4 control-label">Username</label>

                                        <input id="username" type="text" class="form-control" name="username" value="{{ Auth::user()->username }}" required>

                                        @if ($errors->has('username'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                        <input id="password" type="password" class="form-control" name="password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>

                                <div class="form-group">
                                        <button type="submit" class="btn btn-primary">
                                            Update
                                        </button>
                                </div>
                            </form>
                        </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-4">
            <div class="panel panel-default">

                @if( Auth::user()->id == $user->id )
                <div class="panel-body">

                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{route('post.create')}}">
                        {{ csrf_field() }}

                        @if (Session::has('sent'))
                            <p class="alert alert-success">
                                <strong>{{ Session::get( 'sent' ) }}</strong>
                            </p>
                        @elseif (Session::has('error'))
                            <p class="alert alert-danger">
                                <strong>{{ Session::get( 'error' ) }}</strong>
                            </p>
                        @endif
                        <br>

                        <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <textarea id="message"  class="form-control" name="message" value="{{ old('message') }}" required rows="3" placeholder="What's in your mind?"></textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <label style="margin-top:-10px;" for="status_image" class="btn">Attach Photo</label>
                        <input id="status_image" type="file" name="image" style="visibility:hidden;">


                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-10">
                                <button style="margin-top:-100px;" type="submit" class="btn btn-success">
                                    Post
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
                @endif


                <div class="panel-body">
                    @foreach($posts as $post)
                    <div class="jumbotron">
                        @if( $post->image == 'no image' )

                        @else
                        <center>
                            <img src="{{url($post->image)}}" alt="">
                        </center>
                        @endif

                        <p style="font-size:25px;">{{$post->body}}{{$post->image == 'no image'}}</p>
                        <p style="font-size:12px;margin-top:-23px;">{{ date('F d, Y', strtotime($post->created_at)) }}</p>

                        <form class="" action="{{route('post.comment', ['post_id'=>$post->id])}}" method="POST">
                            {{ csrf_field() }}
                            <div class="input-group">
                              <input id="comment" name="comment" type="text" class="form-control" placeholder="Write a comment" aria-describedby="basic-addon1">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Comment</button>
                              </span>
                            </div>
                        </form>
                        <br>
                        <br>
                        <div style="padding:5px;max-width:598px;max-height:184px;overflow-y:scroll" class="comment-box">
                            <br>
                            @foreach( $post->comments as $comment )
                            @if($comment->commentator->id == Auth::user()->id)
                            <p style="font-size:15px;margin-top:-20px;"><strong>You says: &nbsp;</strong>{{$comment->body}}</p>
                            @else
                            <p style="font-size:15px;margin-top:-20px;"><strong>{{$comment->commentator->name}} says: &nbsp;</strong>{{$comment->body}}</p>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>

                <br>
            </div>
        </div>
    </div>
</div>


@endsection
