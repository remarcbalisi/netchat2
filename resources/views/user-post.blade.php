@extends('layouts.app')

@section('content')



                    <div class="jumbotron">
                        @if( $post->image == 'no image' )

                        @else
                        <center>
                            <img src="{{url($post->image)}}" alt="">
                        </center>
                        @endif

                        <p style="font-size:25px;">{{$post->body}}{{$post->image == 'no image'}}</p>
                        <p style="font-size:12px;margin-top:-23px;">{{ date('F d, Y', strtotime($post->created_at)) }}</p>

                        <form class="" action="{{route('post.comment', ['post_id'=>$post->id])}}" method="POST">
                            {{ csrf_field() }}
                            <div class="input-group">
                              <input id="comment" name="comment" type="text" class="form-control" placeholder="Write a comment" aria-describedby="basic-addon1">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Comment</button>
                              </span>
                            </div>
                        </form>
                        <br>
                        <br>
                        <div style="padding:5px;max-width:598px;max-height:184px;overflow-y:scroll" class="comment-box">
                            <br>
                            @foreach( $post->comments as $comment )
                            @if($comment->commentator->id == Auth::user()->id)
                            <p style="font-size:15px;margin-top:-20px;"><strong>You says: &nbsp;</strong>{{$comment->body}}</p>
                            @else
                            <p style="font-size:15px;margin-top:-20px;"><strong>{{$comment->commentator->name}} says: &nbsp;</strong>{{$comment->body}}</p>
                            @endif
                            @endforeach
                        </div>
                    </div>



@endsection
