<?php $__env->startSection('content'); ?>


                <div class="panel-body">

                    <form class="form-horizontal" role="form" method="POST" action="<?php echo e(route('message.new')); ?>">
                        <?php echo e(csrf_field()); ?>


                        <?php if(Session::has('sent')): ?>
                            <p class="alert alert-success">
                                <strong><?php echo e(Session::get( 'sent' )); ?></strong>
                            </p>
                        <?php elseif(Session::has('error')): ?>
                            <p class="alert alert-danger">
                                <strong><?php echo e(Session::get( 'error' )); ?></strong>
                            </p>
                        <?php endif; ?>
                        <br>

                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" required autofocus placeholder="Reciepient email">

                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('message') ? ' has-error' : ''); ?>">

                            <div class="col-md-6">
                                <textarea id="message"  class="form-control" name="message" value="<?php echo e(old('message')); ?>" required rows="8" cols="80" placeholder="Type your message here"></textarea>
                                <?php if($errors->has('message')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('message')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send
                                </button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>