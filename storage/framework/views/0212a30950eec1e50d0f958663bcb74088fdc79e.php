<?php $__env->startSection('content'); ?>


                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div id="chat-box" style="height:332px;width:100%;background-color:#FAFAFA;overflow-y:scroll" class="chat-box">
                                <br>
                                <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if( $message['sender'] == Auth::user()->name ): ?>
                                    <div class="row">
                                        <div style="float:left" class="pull-right msg-body">
                                                <p style="color:black;padding:10px;text-align:left;background-color:#4080ff;border-radius: 10px;width:330px;" class="pull-left"><?php echo e($message['body']); ?></p>

                                            <div style='clear:both;'><!--clear--></div>
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <div class="row">
                                        <div style="float:left;margin-left:20px;" class="msg-body">
                                            <p style="margin-left:10px;padding:10px;color:black;background-color:#e6e6ff;width:330px;border-radius: 10px;"><strong><?php echo e($message['sender']); ?>: &nbsp;&nbsp;</strong><?php echo e($message['body']); ?></p>
                                            <div style='clear:both;'><!--clear--></div>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <div id="new-messages" class="new-messages">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top:5px;padding-left:14px;padding-right:14px;" class="row">
                        <form id="chat-form" class="" action="" method="post">
                            <?php echo e(csrf_field()); ?>

                            <div class="input-group">
                              <input id="message" name="message" type="text" class="form-control" placeholder="Type Message" aria-describedby="basic-addon1">
                              <span class="input-group-btn">
                                  <a href="<?php echo e(route('message.send')); ?>"></a>
                                <button onclick="" class="btn btn-success" type="submit">Send</button>
                              </span>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>


<!-- socket -->
<script type="text/javascript">
    //var socket = io('http://localhost:3000');

    $(function(){

        $("#chat-box").scrollTop($("#chat-box").get(0).scrollHeight);

        // this function will listen to port 300 and get all the data
        // from the authenticated user channel
        var socket = io('http://localhost:3000');
        socket.on("chat.<?php echo e(Auth::user()->id); ?>.<?php echo e($recipient_id); ?>:App\\Events\\SendMessageEvent", function(data){
            console.log(data);
            $("#new-messages").append(function(){
                var to_string = '<div style="float:left;margin-left:20px;" class="msg-body">'+
                '<p style="margin-left:10px;padding:10px;color:black;background-color:#e6e6ff;width:330px;border-radius: 10px;" class=""><strong>'+ data.sender_name +': &nbsp;&nbsp;</strong>'+data.data.body+'</p>'+
                '</div><div style="clear:both;"><!--clear--></div>';
                return to_string;
            });
            $("#chat-box").scrollTop($("#chat-box").get(0).scrollHeight);
        });

        // an ajax script
        // this will get the message data and send it to server
        // in REST
        $("#chat-form").submit(function(evt){
            evt.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            var message = $("#message").val();
            document.getElementById("message").value = "";

            $.ajax({
                url: "<?php echo e(route('message.send')); ?>",
                type: "POST",
                data: {'recipient_id':'<?php echo e($recipient_id); ?>', 'message':message},
                dataType: 'json',       // Data Type of the Transmit
                beforeSend: function (xhr) {
                    // Function needed from Laravel because of the CSRF Middleware
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                success: function(data){
                    console.log(data);

                    $("#new-messages").append(function(){

                        var to_string = '<div class="row"><div style="float:left;" class="pull-right msg-body">'+
                        '<p style="color:black;padding:10px;text-align:left;background-color:#4080ff;border-radius: 10px;width:330px;" class="pull-left">'+data.body+'</p>'+
                        '</div></div><div style="clear:both;"><!--clear--></div>';
                        return to_string;
                    });

                    $("#chat-box").scrollTop($("#chat-box").get(0).scrollHeight);
                },
                error(err){
                    console.log(err.responseText);
                }
            });

        });
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>