<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('APP_NAME', 'NetChat')); ?></title>

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
    </script>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
    <script src="<?php echo e(asset('js/socket.io.js')); ?>"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                        <?php echo e(config('APP_NAME', 'NetChat')); ?>

                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <?php if(Auth::guest()): ?>
                            <li><a href="<?php echo e(route('login')); ?>">Login</a></li>
                            <li><a href="<?php echo e(route('register')); ?>">Register</a></li>
                        <?php else: ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>

                                        <a href="<?php echo e(route('user.view', ['username'=>Auth::user()->username])); ?>">
                                            <img style="vertical-align:top" width="20%" src="<?php echo e(url(Auth::user()->image)); ?>" alt="">
                                            View Profile
                                        </a>

                                        <a href="<?php echo e(route('logout')); ?>"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <?php if(Auth::check()): ?>
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="" action="<?php echo e(route('user.search')); ?>" method="POST">
                                <?php echo e(csrf_field()); ?>

                                <div class="input-group">
                                  <input id="name" name="name" type="text" class="form-control" placeholder="Search on NetChat" aria-describedby="basic-addon1">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Search!</button>
                                  </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div style="height:480px;overflow-y: scroll;" class="panel panel-default">
                        <div class="panel-heading">Friends&nbsp;(&nbsp;&nbsp;<?php echo e(Auth::user()->friends->count()); ?>&nbsp;&nbsp;)</div>

                        <div class="panel-body">
                            <div class="links">
                                <a href="<?php echo e(route('home')); ?>">Create New Message</a>
                                <br>
                                <br>
                                <?php if(!Auth::user()->friends->count()): ?>
                                <p>You have no friends</p>
                                <?php endif; ?>
                                <?php $__currentLoopData = Auth::user()->friends; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $friend): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a href="<?php echo e(route('user.view', ['username'=>$friend->username])); ?>"><?php echo e($friend->name); ?></a>
                                <a style="text-decoration:none" href="<?php echo e(route('message.view', ['user_id'=>$friend->id])); ?>">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;View Messages</a>
                                <p style="font-size:12px;margin-top:-8px;"><?php echo e($friend->email); ?></p>
                                <br>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">Dashboard</div>

                        <div class="panel-body">
                            <li style="list-style-type: none;display:inline-block">
                                <a href="<?php echo e(route('home')); ?>">Home</a>
                            </li>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <li style="list-style-type: none;display:inline-block" class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Friend Requests&nbsp;(&nbsp;<?php echo e($friend_requests->where('is_accepted', false)->count()); ?>&nbsp;) <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <?php if( !$friend_requests->where('is_accepted', false)->count() ): ?>
                                        <li><a href="#">You have no new Friend Request</a></li>
                                        <li class="divider"></li>
                                        <?php else: ?>
                                            <?php $__currentLoopData = $friend_requests->where('is_accepted', false); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $friend_request): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <a href="<?php echo e(route('user.view', ['username'=>$friend_request->requestor_model->username])); ?>">
                                            Friend request from <?php echo e($friend_request->requestor_model->name); ?>

                                            </a>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <li class="divider"></li>
                                        <?php endif; ?>

                                    </li>
                                    <?php $__currentLoopData = $friend_requests->where('is_accepted', true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $friend_request): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a style="color:green;" href="<?php echo e(route('user.view', ['username'=>$friend_request->requestor_model->username])); ?>">
                                        &#10004;Friend request from <?php echo e($friend_request->requestor_model->name); ?>

                                        </a>
                                    </li>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <li style="list-style-type: none;display:inline-block" class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Inbox&nbsp;(&nbsp;<?php echo count($new_messages) ?>&nbsp;) <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <?php if( !count($new_messages) ): ?>
                                    <li>
                                        <li><a href="#">No new Messages</a></li>
                                    </li>
                                    <?php endif; ?>
                                    <?php $__currentLoopData = $new_messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <li>
                                            <a href="<?php echo e(route('message.view', ['user_id'=>$message['id']])); ?>">Message from <?php echo e($message["name"]); ?> (&nbsp;<?php echo e($message["count"]); ?>&nbsp;)</a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <li style="list-style-type: none;display:inline-block" class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Notification&nbsp;(&nbsp;<?php echo e($notifications->where('is_read', false)->count()); ?>&nbsp;) <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <?php if( !$notifications->where('is_read', false)->count() ): ?>
                                    <li>
                                        <a href="#">No new Notifications</a>
                                    </li>
                                    <?php endif; ?>
                                    <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <li>
                                        <a href="<?php echo e(route('notification.view', ['notification_id'=>$notification->id])); ?>">
                                            <?php if( $notification->is_read == true ): ?>
                                            &#10004;&nbsp;
                                            <?php endif; ?>
                                            <?php echo e($notification->message); ?>

                                        </a>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>
                        </div>

                        <br>
        <?php endif; ?>

        <?php echo $__env->yieldContent('content'); ?>
    </div>

</body>
</html>
