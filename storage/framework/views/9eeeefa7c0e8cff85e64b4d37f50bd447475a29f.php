<?php $__env->startSection('content'); ?>



                    <div class="jumbotron">
                        <?php if( $post->image == 'no image' ): ?>

                        <?php else: ?>
                        <center>
                            <img src="<?php echo e(url($post->image)); ?>" alt="">
                        </center>
                        <?php endif; ?>

                        <p style="font-size:25px;"><?php echo e($post->body); ?><?php echo e($post->image == 'no image'); ?></p>
                        <p style="font-size:12px;margin-top:-23px;"><?php echo e(date('F d, Y', strtotime($post->created_at))); ?></p>

                        <form class="" action="<?php echo e(route('post.comment', ['post_id'=>$post->id])); ?>" method="POST">
                            <?php echo e(csrf_field()); ?>

                            <div class="input-group">
                              <input id="comment" name="comment" type="text" class="form-control" placeholder="Write a comment" aria-describedby="basic-addon1">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Comment</button>
                              </span>
                            </div>
                        </form>
                        <br>
                        <br>
                        <div style="padding:5px;max-width:598px;max-height:184px;overflow-y:scroll" class="comment-box">
                            <br>
                            <?php $__currentLoopData = $post->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($comment->commentator->id == Auth::user()->id): ?>
                            <p style="font-size:15px;margin-top:-20px;"><strong>You says: &nbsp;</strong><?php echo e($comment->body); ?></p>
                            <?php else: ?>
                            <p style="font-size:15px;margin-top:-20px;"><strong><?php echo e($comment->commentator->name); ?> says: &nbsp;</strong><?php echo e($comment->body); ?></p>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>