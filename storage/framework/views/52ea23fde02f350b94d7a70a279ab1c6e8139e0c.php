<?php $__env->startSection('content'); ?>

                <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="<?php echo e(route('user.update', ['id'=>Auth::user()->id])); ?>">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img style="display:inline-block;vertical-align:top" src="<?php echo e(url($user->image)); ?>" alt="">
                            <br>
                            <label for="image" class="btn">Update Photo</label>
                            <input id="image" type="file" name="image" style="visibility:hidden;">
                            <h2 style="margin-top:-15px;"><?php echo e($user->name); ?></h2>
                            <h5><?php echo e($user->email); ?></h5>
                            <?php if( $user->is_activated == true ): ?>
                            <a href="<?php echo e(route('admin.user.deactivate', ['user_id'=>$user->id])); ?>">
                                <button id="report-user-btn" type="button" class="btn btn-danger btn-xs">Deactivate User</button>
                            </a>
                            <?php else: ?>
                            <a href="<?php echo e(route('admin.user.activate', ['user_id'=>$user->id])); ?>">
                                <button id="report-user-btn" type="button" class="btn btn-success btn-xs">Activate User</button>
                            </a>
                            <?php endif; ?>

                        </div>

                        <div class="col-md-12">
                                <?php echo e(csrf_field()); ?>

                                <?php echo method_field('put'); ?>


                                <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <label for="name" class="control-label">Name</label>


                                        <input id="name" type="text" class="form-control" name="name" value="<?php echo e(Auth::user()->name); ?>" required autofocus>

                                        <?php if($errors->has('name')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('name')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                </div>

                                <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                        <input id="email" type="email" class="form-control" name="email" value="<?php echo e(Auth::user()->email); ?>" required>

                                        <?php if($errors->has('email')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                        <?php endif; ?>

                                </div>

                                <div class="form-group<?php echo e($errors->has('username') ? ' has-error' : ''); ?>">
                                    <label for="username" class="col-md-4 control-label">Username</label>

                                        <input id="username" type="text" class="form-control" name="username" value="<?php echo e(Auth::user()->username); ?>" required>

                                        <?php if($errors->has('username')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('username')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                </div>

                                <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                        <input id="password" type="password" class="form-control" name="password">

                                        <?php if($errors->has('password')): ?>
                                            <span class="help-block">
                                                <strong><?php echo e($errors->first('password')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>

                                <div class="form-group">
                                        <button type="submit" class="btn btn-primary">
                                            Update
                                        </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="container">

    <div class="row">

        <div class="col-md-8 col-md-offset-4">
            <div class="panel panel-default">

                <div class="panel-body">
                    <?php $__currentLoopData = $user_posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="jumbotron">
                        <button style="margin-bottom:10px;" id="delete-post-btn" type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-post-modal-<?php echo e($post->id); ?>">Delete this Post</button>
                        <!-- Modal -->
                        <div id="delete-post-modal-<?php echo e($post->id); ?>" class="modal fade" role="dialog">
                          <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Are you sure you want to delete this post?</h4>
                              </div>
                              <div class="modal-body">
                                <div class="jumbotron">
                                    <?php if( $post->image == 'no image' ): ?>

                                    <?php else: ?>
                                    <center>
                                        <img src="<?php echo e(url($post->image)); ?>" alt="">
                                    </center>
                                    <?php endif; ?>

                                    <p style="font-size:25px;"><?php echo e($post->body); ?></p>
                                    <p style="font-size:12px;margin-top:-23px;"><?php echo e(date('F d, Y', strtotime($post->created_at))); ?></p>
                                    <form class="" action="<?php echo e(route('admin.post.delete', ['post_id'=>$post->id])); ?>" method="post">
                                        <?php echo e(csrf_field()); ?>

                                        <?php echo method_field('delete'); ?>

                                        <button type="submit" class="btn btn-success">Yes</button>
                                    </form>
                                </div>
                              </div>
                              <div class="modal-footer">

                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                              </div>
                            </div>

                          </div>
                        </div>

                        <?php if( $post->image == 'no image' ): ?>

                        <?php else: ?>
                        <center>
                            <img src="<?php echo e(url($post->image)); ?>" alt="">
                        </center>
                        <?php endif; ?>

                        <p style="font-size:25px;"><?php echo e($post->body); ?><?php echo e($post->image == 'no image'); ?></p>
                        <p style="font-size:12px;margin-top:-23px;"><?php echo e(date('F d, Y', strtotime($post->created_at))); ?></p>

                        <form class="" action="<?php echo e(route('post.comment', ['post_id'=>$post->id])); ?>" method="POST">
                            <?php echo e(csrf_field()); ?>

                            <div class="input-group">
                              <input id="comment" name="comment" type="text" class="form-control" placeholder="Write a comment" aria-describedby="basic-addon1">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Comment</button>
                              </span>
                            </div>
                        </form>
                        <br>
                        <br>
                        <div style="padding:5px;max-width:598px;max-height:184px;overflow-y:scroll" class="comment-box">
                            <br>
                            <?php $__currentLoopData = $post->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($comment->commentator->id == Auth::user()->id): ?>
                            <p style="font-size:15px;margin-top:-20px;"><strong>You says: &nbsp;</strong><?php echo e($comment->body); ?></p>
                            <?php else: ?>
                            <p style="font-size:15px;margin-top:-20px;"><strong><?php echo e($comment->commentator->name); ?> says: &nbsp;</strong><?php echo e($comment->body); ?></p>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>

                <br>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>