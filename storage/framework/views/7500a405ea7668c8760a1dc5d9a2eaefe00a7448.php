<?php $__env->startSection('content'); ?>

                <div class="panel-body">
                    <p style="font-size:17px">Search result for " <?php echo e($searched_user); ?> "</p>

                    <?php $__currentLoopData = $search_users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $search_user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php if( $search_user->id == Auth::user()->id ): ?>
                        <div class="links">
                            <hr>
                            <p>This is you!</p>
                            <br>
                                <div class="row">
                                    <a href="<?php echo e(route('user.view', ['username'=>$search_user->username])); ?>">
                                        <div class="col-md-4">
                                            <img style="display:inline-block" src="<?php echo e(url($search_user->image)); ?>" alt="">
                                        </div>
                                        <div class="col-md-6">

                                                <h3 style="display:inline-block"><?php echo e($search_user->name); ?></h3>
                                                <p><?php echo e($search_user->email); ?></p>

                                        </div>
                                    </a>

                                    <div class="col-md-2">

                                    </div>
                                </div>
                            <hr>
                        </div>
                        <?php else: ?>
                        <div class="links">
                            <hr>
                                <div class="row">
                                    <a href="<?php echo e(route('user.view', ['username'=>$search_user->username])); ?>">
                                        <div class="col-md-4">
                                            <img style="display:inline-block" src="<?php echo e(url($search_user->image)); ?>" alt="">
                                        </div>
                                        <div class="col-md-6">

                                                <h3 style="display:inline-block"><?php echo e($search_user->name); ?></h3>
                                                <p><?php echo e($search_user->email); ?></p>

                                        </div>
                                    </a>

                                    <div class="col-md-2">
                                        <?php if( !$search_user->check_friendship($search_user->id) ): ?>
                                        <a href="<?php echo e(route('request.add', ['user_id'=>$search_user->id])); ?>"><button class="btn btn-primary" type="button" name="button">+Add Friend</button></a>
                                        <?php elseif($search_user->check_friendship($search_user->id)->request_for == Auth::user()->id && $search_user->check_friendship($search_user->id)->is_accepted == false): ?>
                                        <a href="<?php echo e(route('request.accept', ['friend_request_id'=>$search_user->check_friendship($search_user->id)->id])); ?>"><button class="btn btn-primary" type="button" name="button">+Accept</button></a>
                                        <?php elseif($search_user->check_friendship($search_user->id)->is_accepted == false): ?>
                                            <p>&#10004;&nbsp;Friend Request Sent!</p>
                                        <?php elseif($search_user->check_friendship($search_user->id)->is_accepted == true): ?>
                                            <p style="color:green">&#10004;&nbsp;Friends!</p>
                                        <?php endif; ?>
                                        <!-- <?php echo e($search_user->check_friendship($search_user->id)); ?> -->
                                    </div>
                                </div>
                            <hr>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>